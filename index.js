// console.log('Hello World');

let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"]
	},
	talk: function(){
		return this.pokemon[0] + "!" + " I choose you!";
		/*or
		console.log("Pikachu! I choose you!*/
	}
}
console.log(trainer)
console.log("Result of dot notation:")
console.log(trainer.name);
console.log("Result of square bracket notation:");
console.log(trainer["pokemon"]);
console.log("Result of talk method");
console.log(trainer.talk());


function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name);
		target.health -= this.attack
		console.log(target.name + "'s health is now reduced to " + Number(target.health));
		if (target.health <= 0){
			target.faint();
		}
	}
	this.faint = function(){
		console.log(this.name + " fainted.")
	}

}

let pikachu = new Pokemon("Pikachu", 12);
let geodude = new Pokemon("Geodude", 8);
let mewtwo = new Pokemon("Mewtwo", 100);

console.log(pikachu);
console.log(geodude);
console.log(mewtwo);

geodude.tackle(pikachu);
console.log(pikachu);

mewtwo.tackle(geodude);
console.log(geodude)


